#!/usr/bin/env bash

set -eo pipefail

find e2e/ \( -name "*.sh" ! -name "run.sh" \) -print0 | xargs -0L1 -i bash -c 'echo -e "\e[0;92m\e[1m\n\nExecuting: {}\e[0m\n"; {}'
