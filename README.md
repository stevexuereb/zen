# Zen

Simple CLI tool that disable distractions on macOS.

## Installation

1. Make sure you have [rust installed](https://www.rust-lang.org/tools/install)
1. `cargo install zen --git https://gitlab.com/azzsteve/zen.git --tag v0.1.0 `

## Configuration

The configuration for `zen` lives at `$HOME/.zen/config.toml`. Create the
file and add the example below by updating it's contents!

```toml
# List of domains that you want to block
domains = [
    "example.com",
    "example.net",
]
```

## Usage

1. Start blocking: `sudo zen start`
1. Stop blocking: `sudo zen stop`

### Why do I need to use `sudo`

`zen` modifies system files such as `/etc/hosts` file which require root
priveleges.

## DNS over HTTPS (DoH)

[DoH](https://developers.cloudflare.com/1.1.1.1/dns-over-https/) prevents man
in the middle attacks but also prevents from this tool working. When a
browser uses DoH it ignores the `/etc/hosts` file.

---

*Please note that this project is released with a [Contributor Code of Conduct](CODE-OF-CONDUCT.md). By participating in this project you agree to abide by its terms.*
