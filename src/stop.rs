use std::path::Path;

use clap::{App, SubCommand};

use crate::{config::zen_dir, hosts::unblock};

// STOP_CMD is the name of the stop sub command.
pub const STOP_CMD: &str = "stop";

// stop_cmd return a new clap subcommand
pub fn stop_cmd<'a, 'b>() -> App<'a, 'b> {
    SubCommand::with_name(STOP_CMD).about("Stop blocking things")
}

// stop runs the sub command.
pub fn stop() {
    let zen_dir = zen_dir().expect("Failed to get zen directory");

    unblock(&zen_dir, &Path::new("/etc/hosts").to_path_buf()).unwrap();
}
