use clap::{crate_authors, crate_name, crate_version, App};
use start::{start, start_cmd, START_CMD};
use stop::{stop, stop_cmd, STOP_CMD};

mod config;
mod hosts;
mod start;
mod stop;

fn main() {
    let matches = App::new(crate_name!())
        .version(crate_version!())
        .author(crate_authors!())
        .about("Block things to help you focus.")
        .subcommand(start_cmd())
        .subcommand(stop_cmd())
        .get_matches();

    if let Some(_matches) = matches.subcommand_matches(START_CMD) {
        start();
    }

    if let Some(_matches) = matches.subcommand_matches(STOP_CMD) {
        stop();
    }
}
