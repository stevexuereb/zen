use anyhow::{anyhow, Context, Result};
use home::home_dir;
use serde_derive::Deserialize;
use std::{fs, path::PathBuf};

#[derive(Deserialize, PartialEq, Debug)]
pub struct Config {
    pub domains: Vec<String>,
}

// Read the config.toml inside of the specificed zen_dir and deserialize it to a
// Config.
pub fn read_config(zen_dir: &PathBuf) -> Result<Config> {
    let config_path = zen_dir.join("config.toml");

    let config_str = fs::read_to_string(config_path)
        .context("Failed to read configuration from $HOME/zen/config.toml")?;

    let config: Config = toml::from_str(&config_str).context("Marshaling toml string")?;

    Ok(config)
}

// Get the zen directory which zen has full control over, configuration,
// backupfiles, tmpfiles go in here.
pub fn zen_dir() -> Result<PathBuf> {
    let home_dir = match home_dir() {
        Some(p) => p,
        None => return Err(anyhow!("Getting home directory")),
    };

    Ok(home_dir.join(".zen"))
}

#[cfg(test)]
mod tests {
    use fs::create_dir;
    use std::{fs::File, io::Write};
    use tempfile::tempdir;

    use super::*;

    #[test]
    fn test_config_load() {
        let config_str = "domains = [
            \"example.com\",
            \"example.org\",
            \"example.net\",
        ]";

        let want_config = Config {
            domains: vec![
                String::from("example.com"),
                String::from("example.org"),
                String::from("example.net"),
            ],
        };

        let dir = tempdir().unwrap();
        let zen_dir = dir.path().join(".zen");
        create_dir(&zen_dir).expect("creating zen directory");

        let file_path = zen_dir.join("config.toml");
        let mut file = File::create(file_path).expect("creating zen config file");
        file.write_all(config_str.as_bytes()).unwrap();

        let config = read_config(&zen_dir).unwrap();

        assert_eq!(want_config, config);

        drop(file);
        dir.close().unwrap();
    }
}
