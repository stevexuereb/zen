docker-shell:
	docker run --rm -it -v $(PWD):/code -w /code rust:1.50.0

clippy:
ifeq ($(shell rustup component list | grep 'clippy' | grep 'installed' ), )
	# Installing clippy
	rustup component add clippy
endif
	# Run clippy
	cargo clippy --all-targets --all-features -- -D warnings

fmt:
ifeq ($(shell rustup component list | grep 'fmt' | grep 'installed' ), )
	# Installing rustfmt
	rustup component add rustfmt
endif
	# Run rustfmt
	cargo fmt --all -- --check

test:
	cargo test

build:
	cargo build --release

.PHONY: e2e
e2e:
	PATH=$$PATH:$$PWD/target/release/ ./e2e/run.sh
